'use strict';

angular.module('myApp.view1', ['ngRoute', 'ui.bootstrap', 'myApp.attendance'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view1', {
    templateUrl: 'view1/view1.html',
    controller: 'View1Ctrl'
  });
}])

.controller('View1Ctrl', ['$scope', function($scope) {

  $scope.org = {
   "org":[
      {
         "name":"vh1",
         "msg":"msg1",
         "gd":[
            {
               "name":"gd1",
               "st":"NA",
               "cl":"Red"
            },
            {
               "name":"gd2",
               "st":"9:25",
               "cl":"Red"
            }
         ]
      },
      {
         "name":"vh2",
         "msg":"msg2",
         "gd":[
            {
               "name":"gd1",
               "st":"9:15",
               "cl":"Green"
            },
            {
               "name":"gd2",
               "st":"9:20",
               "cl":"Green"
            }
         ]
      },
      {
         "name":"vh3",
         "msg":"msg3",
         "gd":[
            {
               "name":"gd1",
               "st":"9:15",
               "cl":"Green"
            },
            {
               "name":"gd2",
               "st":"9:20",
               "cl":"Green"
            }
         ]
      },
      {
         "name":"vh4",
         "msg":"msg4",
         "gd":[
            {
               "name":"gd1",
               "st":"9:15",
               "cl":"Green"
            },
            {
               "name":"gd2",
               "st":"9:20",
               "cl":"Green"
            }
         ]
      }
   ]
};

}]);
